<?php
/**
 * Init Breadcrumbs.
 *
 * @package Gridd
 */

use Gridd\Grid_Part\Breadcrumbs;

new Breadcrumbs();
