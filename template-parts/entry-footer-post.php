<?php
/**
 * Template part for displaying posts
 *
 * @package Gridd
 * @since 1.0
 */

gridd_the_edit_link();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
