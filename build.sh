rm -Rf gridd
git clone git@gitlab.com:wplemon/gridd.git
cd gridd
npm install && grunt && grunt language

rm -Rf bin
rm -Rf node_modules
rm -Rf vendor
rm -Rf tests
rm -Rf .git

rm -f .browserslistrc
rm -f .editorconfig
rm -f .eslintignore
rm -f .eslintrc.json
rm -f .gitignore
rm -f .gitlab-ci.yml
rm -f .jscsrc
rm -f .jshintignore
rm -f .jshintrc
rm -f build.sh
rm -f composer.json
rm -f composer.lock
rm -f Gruntfile.js
rm -f package-lock.json
rm -f package.json
rm -f phpcs.xml.dist
rm -f phpunit.xml.dist

find . -type f -name '*.css.map' -exec rm {} +

cd ..
rm -f gridd.zip
zip -rq gridd.zip gridd
rm -Rf gridd